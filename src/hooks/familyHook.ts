import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { DataProps } from '../App';
const useFamilyMember = () => {
    const dispatch = useDispatch();
    const [inputs, setInputs] = useState<any>({});
    const [selectedAction, setSelectedAction] = useState<string>("")
    const [addrtype,] = useState(["father", "mother", "child", "grandmother of mother", "grandmother of father", "grandfather of mother", "grandfather of father"])

    const Add = addrtype.map(Add => Add);
    const handleSubmit = (event: any) => {
        if (event) {
            event.preventDefault();
            dispatch.crud[selectedAction](inputs)
            setSelectedAction("")
        }
    }
    const handleInputChange = (event: React.ChangeEvent<any>) => {
        event.persist();
        setInputs((inputs: DataProps) => ({ ...inputs, [event.target.name]: event.target.value }));
    }
    const handleAddrTypeChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        console.clear();
        setInputs((inputs: DataProps) => ({ ...inputs, [event.target.name]: event.target.value }));
    }

    return {
        handleSubmit,
        handleInputChange,
        inputs,
        handleAddrTypeChange,
        Add,
        setSelectedAction,
        selectedAction,
        setInputs

    };
}

export default useFamilyMember;