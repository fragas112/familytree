import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from "styled-components"
import useFamilyMember from './hooks/familyHook';
import { RootState } from './store';

export interface DataProps {
  firstName:string;
  lastName:string;
  dateOfBirth:any;
}

export const Container = styled.div`
    display: flex;
    align-items: center;
    width: 100%;
    color: white;
`
export const ParentWrapper = styled.div`
align-items: center;
    color: white;
    width: 45%;
  `
export const ButtonContainer = styled.div`
display: flex;
place-content: center;
align-items: center;
`
export const FormContainer = styled.form`
display: flex;
    align-items: center;
    justify-content: center;
    padding-top: 5rem;
`

export const Button = styled.button`
    display: flex;
    color: black;
`


export const Tier = styled.div`
    display: flex;
    align-items: center;
    color: white;
    margin: 1rem;
    place-content: center;
`
export const Controls = styled.div`
  display: flex;
  margin: 0.5rem;
`
export const Paragraphs = styled.p`
  margin: 0.2rem;
  cursor: pointer;
`

export const Box = styled.div`
  margin: 0.5rem;
    display: flex;
    flex-direction: column;
    width: 25rem;
    align-items: center;
    color: white;
    background-color: #131A22;
    border-radius: 6rem;
`
function App() {
  const dispatch = useDispatch();
  const name = useSelector((state: RootState) => state?.crud.family)
  const [isShowFather, setIsShowFather] = React.useState(true);
  const [isShowMother, setIsShowMother] = React.useState(true);
  const [editProps, setEditProps] = React.useState<DataProps>();

  const { setInputs, inputs, handleInputChange, handleSubmit, handleAddrTypeChange, Add, setSelectedAction, selectedAction } = useFamilyMember();

  const handleInputEdit = (props: number, data:DataProps) => {
    setSelectedAction("edit")
    console.log(data);
    setEditProps(data)
    setInputs({ ...inputs, props })
  }

  const handleClick = (type: string) => {
    if (type === "father") {
      setIsShowFather(!isShowFather);
    } else {
      setIsShowMother(!isShowMother);

    }
  };
  return (
    <>
      <Container>
        <button onClick={() => handleClick("father")}>Toggle</button>
        {isShowFather ?
          <ParentWrapper>
            <Tier>
              <Box>
                <Paragraphs>GrandMother</Paragraphs>
                {name?.grandParents.mother.grandmother.map((data: DataProps, index: number) => {
                  const body = {
                    key: index,
                    memberType: "grandmother",
                    parentOf: "mother"
                  };
                  return (
                    <React.Fragment key={index}>
                      <Controls>
                        <Controls>
                          <Paragraphs>{data.firstName}</Paragraphs>
                          <Paragraphs>{data.lastName}</Paragraphs>
                          <Paragraphs>{data.dateOfBirth}</Paragraphs>
                        </Controls>
                        <Controls>
                          <Paragraphs onClick={() => handleInputEdit(index, data)}>Edit</Paragraphs>
                          <Paragraphs onClick={() => dispatch.crud.remove(body)}>Delete</Paragraphs>
                        </Controls>
                      </Controls>
                    </React.Fragment>
                  )
                })}
              </Box>
              <Box>
                <Paragraphs>GrandFather</Paragraphs>
                {name?.grandParents.mother.grandfather.map((data: DataProps, index: number) => {
                  const body = {
                    key: index,
                    memberType: "grandfather",
                    parentOf: "mother"
                  };
                  return (
                    <React.Fragment key={index}>
                      <Controls>
                        <Controls>
                          <Paragraphs>{data.firstName}</Paragraphs>
                          <Paragraphs>{data.lastName}</Paragraphs>
                          <Paragraphs>{data.dateOfBirth}</Paragraphs>
                        </Controls>
                        <Controls>
                          <Paragraphs onClick={() => dispatch.crud.remove(body)}>Delete</Paragraphs>
                          <Paragraphs onClick={() => handleInputEdit(index,data)}>Edit</Paragraphs>
                        </Controls>
                      </Controls>
                    </React.Fragment>
                  )
                })}
              </Box>
            </Tier>
            <Tier>
              <Box>
                <Paragraphs>Mother</Paragraphs>
                {name?.mother.map((data: DataProps, index: number) => {
                  const body = {
                    key: index,
                    memberType: "mother"
                  };
                  return (
                    <React.Fragment key={index}>
                      <Controls>
                        <Controls>
                          <Paragraphs>{data.firstName}</Paragraphs>
                          <Paragraphs>{data.lastName}</Paragraphs>
                          <Paragraphs>{data.dateOfBirth}</Paragraphs>
                        </Controls>
                        <Controls>
                          <Paragraphs onClick={() => handleInputEdit(index,data)}>Edit</Paragraphs>
                          <Paragraphs onClick={() => dispatch.crud.remove(body)}>Delete</Paragraphs>
                        </Controls>
                      </Controls>
                    </React.Fragment>
                  )
                })}
              </Box>
            </Tier>
          </ParentWrapper>
          :
          <ParentWrapper></ParentWrapper>
        }
        <button onClick={() => handleClick("mother")}>Toggle</button>
        {isShowMother ?
          <ParentWrapper>
            <Tier>
              <Box>
                <Paragraphs>GrandMother</Paragraphs>
                {name?.grandParents.father.grandmother.map((data: DataProps, index: number) => {
                  const body = {
                    key: index,
                    memberType: "grandmother",
                    parentOf: "father"
                  };
                  return (
                    <React.Fragment key={index}>
                      <Controls>
                        <Controls>
                          <Paragraphs>{data.firstName}</Paragraphs>
                          <Paragraphs>{data.lastName}</Paragraphs>
                          <Paragraphs>{data.dateOfBirth}</Paragraphs>
                        </Controls>
                        <Controls>
                          <Paragraphs onClick={() => handleInputEdit(index,data)}>Edit</Paragraphs>
                          <Paragraphs onClick={() => dispatch.crud.remove(body)}>Delete</Paragraphs>
                        </Controls>
                      </Controls>
                    </React.Fragment>
                  )
                })}
              </Box>
              <Box>
                <Paragraphs>GrandFather</Paragraphs>
                {name?.grandParents.father.grandfather.map((data: DataProps, index: number) => {
                  const body = {
                    key: index,
                    memberType: "grandfather",
                    parentOf: "father"
                  };
                  return (
                    <React.Fragment key={index}>
                      <Controls>
                        <Controls>
                          <Paragraphs>{data.firstName}</Paragraphs>
                          <Paragraphs>{data.lastName}</Paragraphs>
                          <Paragraphs>{data.dateOfBirth}</Paragraphs>
                        </Controls>
                        <Controls>
                          <Paragraphs onClick={() => handleInputEdit(index,data)}>Edit</Paragraphs>
                          <Paragraphs onClick={() => dispatch.crud.remove(body)}>Delete</Paragraphs>
                        </Controls>
                      </Controls>
                    </React.Fragment>
                  )
                })}
              </Box>
            </Tier>
            <Tier>
              <Box>
                <Paragraphs>Father</Paragraphs>
                {name?.father.map((data: DataProps, index: number) => {
                  const body = {
                    key: index,
                    memberType: "father"
                  };
                  return (
                    <React.Fragment key={index}>
                      <Controls>
                        <Controls>
                          <Paragraphs>{data.firstName}</Paragraphs>
                          <Paragraphs>{data.lastName}</Paragraphs>
                          <Paragraphs>{data.dateOfBirth}</Paragraphs>
                        </Controls>
                        <Controls>
                          <Paragraphs onClick={() => handleInputEdit(index,data)}>Edit</Paragraphs>
                          <Paragraphs onClick={() => dispatch.crud.remove(body)}>Delete</Paragraphs>
                        </Controls>
                      </Controls>
                    </React.Fragment>
                  )
                })}
              </Box>
            </Tier>
          </ParentWrapper>
          : 
          <ParentWrapper></ParentWrapper>
          }
      </Container>
      <Tier>
        <Box>

          {name?.child.map((data: DataProps, index: number) => {
            const body = {
              key: index,
              memberType: "child"
            };
            return (
              <React.Fragment key={index}>
              <Controls>
                  <Controls>
                    <Paragraphs>{data.firstName}</Paragraphs>
                    <Paragraphs>{data.lastName}</Paragraphs>
                    <Paragraphs>{data.dateOfBirth}</Paragraphs>
                  </Controls>
                  <Controls>
                    <Paragraphs onClick={() => handleInputEdit(index,data)}>Edit</Paragraphs>
                    <Paragraphs onClick={() => dispatch.crud.remove(body)}>Delete</Paragraphs>
                  </Controls>
                </Controls>
              </React.Fragment>
            )
          })}
        </Box>

      </Tier>
      <ButtonContainer>
        <Button onClick={() => setSelectedAction("add")} >
          Add a Family member
        </Button>
      </ButtonContainer>


      <FormContainer onSubmit={handleSubmit}>
        {selectedAction === "add" ?
          <>
            <select
              onChange={e => handleAddrTypeChange(e)} name="memberType" >
              {<option>Select family type
              </option>}
              {
                Add.map((family, key) => <option key={key} value={inputs.familyMember}>{family}
                </option>)
              }
            </select >
            <label>First Name</label>
            <input type="text" name="firstName" onChange={handleInputChange} value="" required />
            <label>Last Name</label>
            <input type="text" name="lastName" onChange={handleInputChange} value="" required />
            <label>BirthDate</label>
            <input onChange={handleInputChange} name="date" type="date" placeholder="date" value="" required />
            <input type="submit" />
          </> :
          null
        }
        {selectedAction === "edit" ?
          <>
            <label>First Name</label>
            <input type="text" name="firstName" onChange={handleInputChange} value={editProps.firstName} required />
            <label>Last Name</label>
            <input type="text" name="lastName" onChange={handleInputChange} value={editProps.lastName} required />
            <label>BirthDate</label>
            <input onChange={handleInputChange} name="date" type="date" placeholder="date" value={editProps.dateOfBirth} required />
            <input type="submit" />
          </> :
          null
        }
      </FormContainer>

    </>
  );
}

export default App;
