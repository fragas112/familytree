const crud = {
    state: {
        family: {
            mother: [],
            father: [],
            grandParents: {
                mother: {
                    grandmother: [],
                    grandfather: [],
                },
                father: {
                    grandmother: [],
                    grandfather: [],
                }
            },
            child: []
        }
    },
    reducers: {
        add(state: any, payload: any) {
            if (payload.memberType === "father" || payload.memberType === "mother" || payload.memberType === "child") {
                return {
                    ...state,
                    family: {
                        ...state.family,
                        [payload.memberType]: [
                            ...state.family[payload.memberType],
                            {
                                firstName: payload.firstName,
                                lastName: payload.lastName,
                                dateOfBirth: payload.date
                            }
                        ]
                    }
                }
            } else {
                const destructFamilyParents = payload.memberType.slice(15);
                const destructFamilyGrandParents = payload.memberType.slice(0, 11);
                return {
                    ...state,
                    family: {
                        ...state.family,
                        grandParents: {
                            ...state.family.grandParents,
                            [destructFamilyParents]: {
                                ...state.family.grandParents[destructFamilyParents],
                                [destructFamilyGrandParents]: [
                                    ...state.family.grandParents[destructFamilyParents][destructFamilyGrandParents],
                                    {
                                        firstName: payload.firstName,
                                        lastName: payload.lastName,
                                        dateOfBirth: payload.date
                                    }],
                            }
                        }
                    }
                }
            }
        },
        remove(state: any, payload: { parentOf: string, memberType: string, key: number }) {
            console.log(payload)
            if (payload.memberType === "father" || payload.memberType === "mother" || payload.memberType === "child") {
                return {
                    ...state,
                    family: {
                        ...state.family,
                        [payload.memberType]: [
                            ...state.family[payload.memberType].slice(0, payload.key),
                            ...state.family[payload.memberType].slice(payload.key + 1)
                        ]
                    }
                }
            } else {
                return {
                    ...state,
                    family: {
                        ...state.family,
                        grandParents:{
                            ...state.family.grandParents,
                            [payload.parentOf]:{
                                ...state.family.grandParents[payload.parentOf],
                                [payload.memberType]:[
                                    ...state.family.grandParents[payload.parentOf][payload.memberType].slice(0, payload.key),
                                    ...state.family.grandParents[payload.parentOf][payload.memberType].slice(payload.key + 1)
                                ]
                            }
                        }
                    }
                }
            }

        },
        edit(state: any, payload: any) {
            console.log(payload)
            if (payload.memberType === "father" || payload.memberType === "mother" || payload.memberType === "child") {
                return {
                    ...state,
                    family: {
                        ...state.family,
                        [payload.memberType]: [
                            ...state.family[payload.memberType].map(((todo: any, index: number) => index === payload.data ?
                                // transform the one with a matching id
                                {
                                    ...todo,
                                    firstName: payload.firstName,
                                    lastName: payload.lastName,
                                    dateOfBirth: payload.date
                                }
                                :
                                // otherwise return original todo
                                todo
                            ))
                        ]

                    }
                }
            } else {
                const destructFamilyParents = payload.memberType.slice(15);
                const destructFamilyGrandParents = payload.memberType.slice(0, 11);
                return {
                    ...state,
                    family: {
                        ...state.family,
                        grandParents: {
                            ...state.family.grandParents,
                            [destructFamilyParents]: {
                                ...state.family.grandParents[destructFamilyParents],
                                [destructFamilyGrandParents]: [
                                    ...state.family.grandParents[destructFamilyParents][destructFamilyGrandParents].map((todo: any, index: number) =>
                                        index === payload.data ?
                                            {
                                                ...todo,
                                                firstName: payload.firstName,
                                                lastName: payload.lastName,
                                                dateOfBirth: payload.date
                                            }
                                            : todo
                                    ),
                                ],
                            }
                        }
                    }
                }
            }

        }
    }
};

export default crud;
