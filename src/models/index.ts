import { Models } from '@rematch/core'
import crud from './crud'


export interface RootModel extends Models<any> {
    crud: typeof crud
}

export const models: RootModel = { crud }
